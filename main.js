/**
 * 
 */
var source = null;
var chartT;
var synchro = false;

function main(){
	window.onload = event;

	Highcharts.setOptions({ 
		time: {timezone: 'Europe/Warsaw' },
		global : {useUTC : false}
	});

	chartT = new Highcharts.Chart({
 		chart:{ 
			renderTo : 'chart1' },
		title: { 
			text: 'BME280 Sensor' },
	 	series: [],
		plotOptions: {
	    	line: { animation: false, dataLabels: { enabled: true }}
		},
	  	xAxis: { 
			type: 'datetime',
	    	dateTimeLabelFormats: { second: '%H:%M:%S' }
	  	},
	  	yAxis: [
		{
   			title: { text: 'Temperature [C]' }
		},
		{
   			title: { text: 'Pressure [hPa]' },
		    opposite: true
		},
		{
   			title: { text: 'Humidity [%]' }
		}]
	});	
}
 
function event() {
	if(typeof(EventSource) !== "undefined") {
		source = new EventSource("event");
		source.addEventListener('message', onMessage, false);
		source.addEventListener("open", function() {
    		console.log("Connection was opened.");
		}, false);
  	}
}

function onMessage(ssEvent) {
	var data = JSON.parse(ssEvent.data);
	chartParse(data);
}

function eventClose() {
	if (source.OPEN) {
		source.close();
		source = null;
	}
}

function eventReStart() {
	if (source === null) {
		event();
	}
}

function measure() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
    	if (this.readyState === 4 && this.status === 200) {
        	var data = JSON.parse(this.responseText);
        	chartParse(data);
    	}
	};
	var query = '/val';
	xhr.open('GET', query, true);
	xhr.send(null);
}

function chartParse(data) {
	var x = data.x;
	var y = data.y;
	var type = data.type - 1;

	if (type === 0) synchro = true;
	if (synchro){
		if (typeof chartT.series[type] === 'undefined') {
			if ((type % 3) === 0){
				chartT.addSeries({                        
					name: 'Temperature:'+ data.ip ,
					yAxis: 0,
	    			data: [],
	    			showInNavigator: true});}
			else if ((type % 3) === 1){
				chartT.addSeries({                        
					name: 'Pressure:'+ data.ip,
					yAxis: 1,
	    			data: [],
	    			showInNavigator: true});}
			else if ((type % 3) === 2){
				chartT.addSeries({                        
					name: 'Humidity:'+ data.ip,
					yAxis: 0,
	    			data: [],
					showInNavigator: true});}
		}
		if (chartT.series[type].data.length > 40) {
			chartT.series[type].addPoint([x, y], true, true, true);
		} else {
			chartT.series[type].addPoint([x, y], true, false, true);
		}
	}
}
