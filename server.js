
var fs = require('fs');
var http = require('http');
var qs = require('querystring');
var util = require('util');
var setup ='';

var clients = [];
var messages = [];

async function sendAllMessegesEvents(messages, client) {
	messages.forEach( function (message_send) {
		//console.log('Event:%s=>%s', client.clientAddress, message_send);	  
		client.respond.write(message_send);
	});	
}

async function sendEventsToAll(message) {
	clients.forEach( function(client) {
		console.log('Event:%s=>%s', client.clientAddress, message);	  
		client.respond.write(message);
	  	});
}
/* debug purpose only
var refreshRate = 10000; // in milliseconds
setInterval( function() {
	var type = 1;
	var x = Date.now();
	var y = Math.random();
	var message = util.format( 'data: {"ip": "xxx", "type": %d, "x": %s, "y": %s}\n\n', type, x, y);
	//var myJSON = JSON.stringify(obj); // other way
	if (messages.push(message) > 40) {
		messages.shift();
	}
	sendEventsToAll(message);
}, refreshRate);
*/
function getParser(respond, request, get) {

	if (request.url ==='/event') {
	    respond.writeHead(200, {
	    	'Content-Type': 'text/event-stream',
	    	'Cache-Control': 'no-cache',
	    	'Connection': 'keep-alive',
		 });
	    
	    var clientAddress = request.connection.remoteAddress;
	    var clientId = Date.now();
	    var newClient = {
	    		id: clientId,
	    	    respond: respond,
	    	    clientAddress: clientAddress,
	    };
	    clients.push(newClient);
		request.on('close', function() {
			console.log("Connection closed: %s", clientId);
			clients = clients.filter( function(c) {
				return c.id !== clientId;
		    });
			return;
		});
		sendAllMessegesEvents(messages, newClient);
	    return;
    }    
	if (request.url.substring(0,6) ==='/setup') {
		setup = request.url.substring(7);
		console.log("Setup: %s", setup);
		request.url = "/";
	}
	if (get.data) {
	    respond.writeHead(200, {'Content-Type': 'text/html'});
	    if (setup ===''){
        	respond.end('No config');
        }else{
        	respond.end(setup);
        	setup ='';
        }
    	var message = get.data;
    	if (messages.push(message) > 40) {
    		messages.shift();
    	}
    	sendEventsToAll(message);
    	return;
	}
		
	var file = request.url.substring(1);
	if (file === '') { file = 'index.html'; }
	console.log('get URL:%s', file);
	
    fs.readFile(file, function( error, content ){
    	if (error) {
    	    respond.writeHead(200, {'Content-Type': 'text/html'});
            respond.end('File not exist');    		
    		return console.log(error);
    	}
    	if (file.split('.').pop()==='js') {
		    respond.writeHead(200, {'Content-Type': 'application/x-javascript'});
	    }else{
   	    	respond.writeHead(200, {'Content-Type': 'text/html'});
    	}
        respond.write(content);
    	respond.end();
    });
}

function postParser(respond, post) {
	respond.writeHead(200, {'Content-Type': 'text/html'});
    if (post.message) {
        respond.end('OK');
    	return console.log('Post message request: %s', post.message);
    }
}

var server = http.createServer(function handler(request, respond) {

    var queryData = "";
        
	if (request.method === 'POST') {
		request.on('data', function (data) {
			queryData += data;
		});
        
		request.on('end', function() {    	
			var post = qs.parse(queryData);
			postParser(respond, post);  
		});
		return;
	} 
	if (request.method === 'GET') {
		request.on('data', function (data) {
			queryData += data;
		});
        
		request.on('end', function() {
			var get = qs.parse(queryData);
			getParser(respond, request, get);  
		});		
		return;
	}
	console.log('Unsupported:%s', request);
});

server.listen(80);

server.on('listening', function()  {
	console.log('Server running at http://%s:%d\n', server.address().address, server.address().port);
});

